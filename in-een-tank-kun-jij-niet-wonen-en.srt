1
00:00:00,000 --> 00:00:03,780
 I am here because I want to show my support for Johannes, who is squatted

2
00:00:03,780 --> 00:00:08,519
and has been arrested for weeks, and I want to show everyone that I

3
00:00:08,519 --> 00:00:14,160
support Johannes. I have seen with great pleasure a number of years ago

4
00:00:14,160 --> 00:00:19,080
how that squatters' movement has developed itself in incredibly creative ways, and with very good

5
00:00:19,080 --> 00:00:22,140
justifications, and how they did incredibly 

6
00:00:22,140 --> 00:00:24,960
nice actions without any violence.

7
00:00:24,960 --> 00:00:28,146
I have also seen how this was reacted to with violence, and how

8
00:00:28,146 --> 00:00:31,590
an increasing escalation developed, which did not start with the squatters

9
00:00:31,590 --> 00:00:35,820
but with the legal authority. That the squatters now want to 

10
00:00:35,820 --> 00:00:39,059
defend themselves against that kind of attacks I can very well imagine.

11
00:00:39,059 --> 00:00:43,829
We are now here protesting peacefully. 

12
00:00:43,829 --> 00:00:47,861
If they start driving into us, I can't tell you what I'll do.

13
00:00:47,861 --> 00:00:51,420
You can't let all this be done to you, it's crazy! Are we living in a policestate or 

14
00:00:51,420 --> 00:00:56,300
under the rule of law? Because that's something I want to fight for.

15
00:00:59,820 --> 00:01:29,249
[Music]

16
00:01:30,680 --> 00:01:34,797
35 years of welfare state, has not solved the housing shortage

17
00:01:34,797 --> 00:01:40,310
large groups of the population live in cramped or too small houses, and although the

18
00:01:40,310 --> 00:01:45,850
government has recognized the right to housing of young people, they are they very last ones who will be served

19
00:01:45,890 --> 00:01:48,642
There is no money, no land but above all,

20
00:01:48,642 --> 00:01:53,950
there's no political will to make housing a priority. The cities turn into slums. Buildings are prepared to be demolished.

21
00:01:57,130 --> 00:02:00,767
Speculators make big money on the scarce housing.

22
00:02:00,767 --> 00:02:07,710
Instead of building affordable homes, expensive offices and luxury flats are being built

23
00:02:15,400 --> 00:02:22,750
Demonstrations and campaigns for a different housing policy

24
00:02:22,750 --> 00:02:28,980
are met with broken promises from the authorities.

25
00:02:32,020 --> 00:02:36,455
The only thing Amsterdam isnt's short of is people looking for housing.

26
00:02:36,455 --> 00:02:41,264
At this moment, it can be said that 1 in 6 Amsterdam's households

27
00:02:41,264 --> 00:02:44,470
are currently looking for a home.

28
00:02:44,470 --> 00:02:48,244
Hereby our letter of demands and our letter to the national government, I'd say,

29
00:02:48,244 --> 00:02:53,130
in the hope that you do something with it quickly and effectively.

30
00:02:55,140 --> 00:03:01,092
I don't think the question is whether you should tackle these kinds of questions or not.

31
00:03:01,092 --> 00:03:03,613
The question is how to approach them.

32
00:03:03,613 --> 00:03:09,489
I am willing to discuss this at any time and I consider this meeting part of that conversation.

33
00:03:09,489 --> 00:03:14,638
And I think I can only conclude that by saying that

34
00:03:14,638 --> 00:03:21,014
I hope that both concerning the dealings in Amsterdam,

35
00:03:21,014 --> 00:03:26,226
and the firm stance we take in The Hague,

36
00:03:26,226 --> 00:03:30,602
that the Amsterdam municipal government will be able to do this 

37
00:03:30,602 --> 00:03:36,360
not in opposition to the groups that made these demands, but together with them. Thank you.

38
00:03:36,790 --> 00:03:52,240
[Music]

39
00:03:52,240 --> 00:04:00,785
At the moment Paul opens the door, which is clearly visible,

40
00:04:00,785 --> 00:04:06,908
he drives away his car, then the barricading group gets out and

41
00:04:06,908 --> 00:04:14,439
get to the house.

42
00:04:28,759 --> 00:04:31,986
The first squatting action took place in the late 1960s

43
00:04:31,986 --> 00:04:36,577
since then, thousands of vacant houses have been added to the housing stock.

44
00:04:36,577 --> 00:04:39,899
At the beginning, mainly nailed-up buildings are squatted.

45
00:04:39,899 --> 00:04:44,129
Those houses, in case they would not have been squatted,

46
00:04:44,129 --> 00:04:49,680
would have remained empty for many years.

47
00:04:49,680 --> 00:04:54,712
Squatters are gradually turning to larger and more expensive properties,

48
00:04:54,712 --> 00:04:57,785
these houses are ideal for living in groups.

49
00:04:57,785 --> 00:05:02,330
This way, squatting offers the chance to experiment with alternative ways of living.

50
00:05:02,370 --> 00:05:04,560
But whether you live in a group or alone

51
00:05:04,560 --> 00:05:08,060
it remains uncertain for how long you will be able to stay.

52
00:05:08,060 --> 00:05:09,837
Although squatting is not illegal,

53
00:05:09,837 --> 00:05:12,208
an owner can go to court to have you evicted.

54
00:05:12,208 --> 00:05:15,380
However, the owner needs to know the names of the squatters.

55
00:05:15,380 --> 00:05:19,815
Recently however, judges have decided to evict without knowing surnames

56
00:05:19,815 --> 00:05:23,339
as they are applying the new Vacancy Law before it has been enacted.

57
00:05:23,339 --> 00:05:27,509
In most cases squatting will become illegal under this new law.

58
00:05:27,509 --> 00:05:30,152
But it was not that difficut a few years ago,

59
00:05:30,152 --> 00:05:33,150
when Margreet squatted a boarded up house in Pijp.  

60
00:05:33,150 --> 00:05:38,342
Well, the floor had been completely opened up with a saw,

61
00:05:38,342 --> 00:05:44,819
the windows were boarded shut,

62
00:05:44,819 --> 00:05:53,409
in the alcove was a stack of glass and a stack of wood, the former windows.

63
00:05:53,409 --> 00:05:58,028
The municipal home inspectors had demolished the fuse box

64
00:05:58,028 --> 00:06:01,509
and pulled the wiring out of the walls.

65
00:06:01,509 --> 00:06:05,334
The toilet bowl was broken, the drainage pipe was gone.

66
00:06:05,334 --> 00:06:08,750
Refurbishing took a fair bit of work on the place.

67
00:06:08,750 --> 00:06:11,439
- Are there many houses like this in the area?

68
00:06:11,439 --> 00:06:13,864
Yes, especially here in the Gerard Douplein neighbourhood, quite a lot of places 

69
00:06:13,864 --> 00:06:17,704
were boarded up and then squatted.

70
00:06:17,704 --> 00:06:25,013
Personally, I've heard from the people that they think it is fine,

71
00:06:25,013 --> 00:06:28,180
they don't like to see boarded up windows either.

72
00:06:28,180 --> 00:06:31,990
Are there still many of these condemned houses left to be squatted?

73
00:06:31,990 --> 00:06:37,120
No, because the municipality has finally changed that policy.

74
00:06:37,120 --> 00:06:40,509
It took years for them to figure out how ridiculous it was.

75
00:06:40,509 --> 00:06:45,825
First moving the people out, then wrecking the houses.

76
00:06:45,825 --> 00:06:55,748
But in the end because people had nothing else or lived in a tiny attic, they squatted 

77
00:06:55,748 --> 00:07:01,110
and renovated these houses.

78
00:07:01,779 --> 00:07:05,510
Most squatters in Amsterdam live as quietly as Margreet,

79
00:07:05,510 --> 00:07:10,420
but if you are unlucky you will have to defend your house from the thugs.

80
00:07:10,420 --> 00:07:15,330
Sometimes you can succeed defending the house by organizing with people.

81
00:07:35,940 --> 00:07:41,088
It is not just private landlords who send thugs over.

82
00:07:41,088 --> 00:07:48,180
In 1978, the council decided to evict a set of houses in the Kinkerbuurt.

83
00:07:48,180 --> 00:07:52,893
According to council's architect, there was a risk of collapse in the building.

84
00:07:52,893 --> 00:07:57,139
The neighborhood's architect came to a completely different conclusion.

85
00:08:08,820 --> 00:08:14,130
The city hall refuses to negotiate and sent over the ME (riot cops). 

86
00:08:15,790 --> 00:08:21,721
The squatters who formed a cordon around the buildings that got broken by force.

87
00:08:21,721 --> 00:08:26,690
This was how the last bit of faith in The City Council disappeared.

88
00:08:26,690 --> 00:08:57,080
[Music]

89
00:09:04,380 --> 00:09:09,155
The fear of more evictions and an upcoming anti-squat law

90
00:09:09,155 --> 00:09:14,864
make the squatters organize themselves better in many neighborhoods.

91
00:09:14,864 --> 00:09:21,859
KSU (Squatting Advice Hour) and squatting cafes are becoming increasingly important

92
00:09:21,859 --> 00:09:26,682
as a meeting place but also as place to organize actions.

93
00:09:26,682 --> 00:09:32,314
Via the telephone alarm list , squatters can be called up quickly.

94
00:09:32,314 --> 00:09:39,570
The magazine from the squatters also provides information, although at a slower pace.

95
00:09:56,500 --> 00:10:02,359
- What do you think of squatter?
 - What do you mean by squatters?

96
00:10:02,359 --> 00:10:07,531
The people on the Keizersgracht.
- They are not squatters.

97
00:10:07,531 --> 00:10:12,560
They just need a roof over their head like everyone else.

98
00:10:12,560 --> 00:10:17,590
They should turn all those empty office blocks into houses.

99
00:10:17,590 --> 00:10:25,710
But you won't catch politicians living in slums. They do so little for the working class.

100
00:10:25,710 --> 00:10:30,135
Better organization makes it possible to squat large properties under speculation.

101
00:10:30,135 --> 00:10:33,070
6 empty buildings are taken over on Keizersgracht area

102
00:10:33,070 --> 00:10:36,720
and became known for the name the Grote Keizersgracht (Big Emperor)

103
00:10:36,720 --> 00:10:39,416
Rob is one of the first residents.

104
00:10:39,416 --> 00:10:41,712
- How did  become a squatter?

105
00:10:41,712 --> 00:10:47,910
Well, I was living at home until my parents moved to the south of the country.

106
00:10:47,910 --> 00:10:52,048
I was studying in Amsterdam and I had a job here so,

107
00:10:52,048 --> 00:10:57,140
I started looking for a room. It took ages to find a tiny place.

108
00:10:57,140 --> 00:11:03,603
I finally found a place paying 300 Gilders a week, excluding gas and electricity.

109
00:11:03,603 --> 00:11:10,724
A friend of mine had it worse, he moved half a dozen times from one tiny room to another.

110
00:11:10,724 --> 00:11:14,649
Another friend was still living with his parents.

111
00:11:14,649 --> 00:11:19,703
The three of us knew each other from school, so we got together

112
00:11:19,703 --> 00:11:23,244
and decided to squat something in Amsterdam.

113
00:11:23,244 --> 00:11:28,004
We wanted to squat a place being kept empty by speculators.

114
00:11:28,004 --> 00:11:33,022
We liked the idea, the buildings are empty and the get no use.

115
00:11:33,022 --> 00:11:38,466
Then one night we just happened to come past these empty buildings.

116
00:11:38,466 --> 00:11:41,345
So we went to the KSU and told them

117
00:11:41,345 --> 00:11:46,547
"These 6 houses are empty. Can you help us out squatting them?"

118
00:11:46,547 --> 00:11:50,863
We soon had 40 or 50 people ready to join the squat.

119
00:11:50,863 --> 00:11:56,350
With the help of experienced people, we moved in in November 1978.

120
00:11:56,350 --> 00:11:58,957
On behalf of the owners, a spy, Paul Van Wissen,

121
00:11:58,957 --> 00:12:02,189
infiltrates the squat and leaks the names of the squatters.

122
00:12:02,189 --> 00:12:03,840
The judge orders the eviction.

123
00:12:03,840 --> 00:12:06,752
For the first time, the squatters disobey a court order,

124
00:12:06,752 --> 00:12:09,820
placing the right of housing above private property rights.

125
00:12:09,820 --> 00:12:12,309
We just decided that we had had enough,

126
00:12:12,309 --> 00:12:16,859
we would not accept to let this house remain empty for another 2 years.

127
00:12:16,859 --> 00:12:21,116
We would stay and live here. We made everyone aware that we will not move out.

128
00:12:21,116 --> 00:12:24,180
Among others, we placed barricades at doors and windows.

129
00:12:24,180 --> 00:12:29,820
The idea is that for once we're not going to be on the defensive, but to be offensive.

130
00:12:29,820 --> 00:12:35,542
This is change of attitude. Frankly, we will not let ourselves be swept away from here.

131
00:12:35,542 --> 00:12:38,049
We are going to drive the police away.

132
00:12:38,049 --> 00:12:44,078
That may sound weird at first, but our whole strategy is based on us taking the initiative.

133
00:12:44,078 --> 00:12:47,748
We are not going to be led to the slaugther like lambs.

134
00:12:47,748 --> 00:12:53,500
It's a matter of principles,  I'm not going to give it up and say, oh well, that's it.

135
00:12:53,500 --> 00:12:57,973
Propaganda becomes effective and different form of solidarity arrive.

136
00:12:57,973 --> 00:13:01,029
Grote Keizersgracht is transformed into a fort,

137
00:13:01,029 --> 00:13:05,797
it becomes a symbol for thousands of the victims from the housing crisis.

138
00:13:05,797 --> 00:13:09,357
It does not only becomes a symbol for acquiring a house,

139
00:13:09,357 --> 00:13:12,574
but also a way of showing that this is a red line.

140
00:13:12,574 --> 00:13:15,090
The squatters prepare for a hard fight.

141
00:13:16,700 --> 00:13:19,919
[Music]

142
00:13:22,840 --> 00:13:33,609
[Music] No, no, we're not going to go!

143
00:13:34,500 --> 00:13:41,616
Good afternoon, this is still Radio Free Keizersgracht on 102Mhzs FM.

144
00:13:41,616 --> 00:13:50,404
We are going to play some music, but I want  to make a call to everyone in Amsterdam.

145
00:13:50,404 --> 00:13:57,255
If you have any spare blankets or records, please bring them here.

146
00:13:57,255 --> 00:14:02,290
We need them badly. We only have 3 records left.

147
00:14:02,290 --> 00:14:09,480
And I would like thank that neighbor who just bought a bag of chips.

148
00:14:09,480 --> 00:14:17,200
This record is for him and I hope more people follow this example.

149
00:14:17,200 --> 00:14:23,498
Of course, it's a gradual process, we didn't suddenly come together and decided to resist.

150
00:14:23,498 --> 00:14:28,398
It was more spontaneous.  I saw at Kinkerbuurt how people simply stood

151
00:14:28,398 --> 00:14:34,162
in front of the house and they just got violent beaten and moved away by the cops.

152
00:14:34,162 --> 00:14:39,804
But these squatters didn't do anything further and this happened multiple times.

153
00:14:39,804 --> 00:14:42,772
So we decided we need to defend ourselves,

154
00:14:42,772 --> 00:14:46,532
we are not going to be forced out of the way anymore.

155
00:14:46,532 --> 00:14:51,375
Now it turns out you need to use violence, they leave you no choice.

156
00:14:51,375 --> 00:14:57,173
I don't see any other option when talking and passive resistance get you nowhere.

157
00:14:57,173 --> 00:15:00,639
It's only when you hit back that they'll listen.

158
00:15:00,639 --> 00:15:05,703
Whereas in fact, violence is the consequence of  the housing shortage.

159
00:15:05,703 --> 00:15:13,367
The Keizersgracht has shown that you've got to go on with the struggle to the end.

160
00:15:13,367 --> 00:15:19,538
We could be talking for years, but that does not resolve anything.

161
00:15:19,538 --> 00:15:26,560
You don't have to do anything else but to squat the house and stick it out.

162
00:15:26,560 --> 00:15:31,440
You cannot leave as soon as they try to get you out,

163
00:15:31,440 --> 00:15:35,575
because nothing else will happen afterwards.

164
00:15:35,575 --> 00:15:44,231
When you look deep into these topics, you realize it's a systemic problem, from out society.

165
00:15:44,231 --> 00:15:48,757
Likewise property rights or housing speculation.

166
00:15:48,757 --> 00:15:55,177
There's people making a lot a money and could change this situation.

167
00:15:55,177 --> 00:16:01,517
But if you try to change yourself, you only find a lot of violence.

168
00:16:01,517 --> 00:16:09,321
For instance, at the events of the Grote Keizersgracht, i was handed out a helmet

169
00:16:09,321 --> 00:16:16,585
and even I was not conformtable  with that in my head, I honestly needed it.

170
00:16:16,585 --> 00:16:25,609
Otherwise, they hit you were it hurts the most. In the end, yes, you need to protect yourself.

171
00:16:25,609 --> 00:16:29,960
Now, I kept a change of old clothes ready and

172
00:16:29,960 --> 00:16:37,520
every morning when the phone rings I think for myself "Oh God, this is it".

173
00:16:37,520 --> 00:16:42,991
There are 54.000 people on the housing list in Amsterdam.

174
00:16:42,991 --> 00:16:49,133
Meanwhile, whole buildings stand empty, rented flats are sold of

175
00:16:49,133 --> 00:16:54,622
f, tenants are intimidated and speculators make fortunes.

176
00:16:54,622 --> 00:17:01,205
Mayor Pollack  and the City hall decided to evict the squatters from

177
00:17:01,205 --> 00:17:08,177
Grote Keizersgracht but they won't do anything to stop the speculators.

178
00:17:08,177 --> 00:17:12,263
The housing shortage must be tackled now.

179
00:17:12,263 --> 00:17:14,860
Housing yes! Evictions no!

180
00:17:19,109 --> 00:17:22,812
By seeing resistance grow in the last years, I think about the beginning.

181
00:17:22,812 --> 00:17:26,120
At one of the early meetings there were only about 10 of us left.

182
00:17:26,120 --> 00:17:31,067
Everyone felt it was hopeless and we'd better pack it in, since we were in a desperate situation.

183
00:17:31,067 --> 00:17:34,841
But we finally decided that the 10 of us would continue with the struggle.

184
00:17:34,841 --> 00:17:36,683
And now look at us! It's incredible.

185
00:17:36,683 --> 00:17:38,389
- How do you explain the results?

186
00:17:38,389 --> 00:17:41,861
I think people have just got fed up with not having decent housing.

187
00:17:41,861 --> 00:17:44,535
Everyone is suffering on daily basis this situation.

188
00:17:44,535 --> 00:17:47,884
You get evicted if you don't want to pay the raises in your rent.

189
00:17:47,884 --> 00:17:52,181
People live in slums, you meet people living in garages paying 400 Gilders a month,

190
00:17:52,181 --> 00:17:55,619
with no water, no gas, no electricity, nothing. It's unbelievable.

191
00:17:55,619 --> 00:17:58,030
People have had enough and they're showing it.

192
00:17:58,030 --> 00:18:01,820
This situation has to change, everyone should be able to afford a house.

193
00:18:01,820 --> 00:18:08,640
OGEM shares at knock-down prices!

194
00:18:08,640 --> 00:18:12,851
Special share issue!

195
00:18:12,851 --> 00:18:16,325
OGEM goes burst!

196
00:18:16,325 --> 00:18:19,800
Final clearance!

197
00:18:19,800 --> 00:18:46,500
[Music]

198
00:18:46,500 --> 00:18:54,962
For the time being, the Council do not dare to evict the squatters.

199
00:18:54,962 --> 00:19:03,460
The squatters are been harassed by the cops by every mean possible.

200
00:19:03,460 --> 00:19:09,589
People making innocent smoke bombs are arrested.

201
00:19:09,589 --> 00:19:18,780
People get charged with criminal offenses for filling up sacks with sand

202
00:19:18,780 --> 00:19:23,380
. Police turn out in mass and force.

203
00:19:23,380 --> 00:19:31,767
They arrested someone over there, I went to see what was going on and they arrested me.

204
00:19:31,767 --> 00:19:34,660
I have no relation with this. 

205
00:19:34,660 --> 00:19:39,273
- What are the police doing here? What should they be doing?

206
00:19:39,273 --> 00:19:45,475
- They should be taking over the capitalis' properties that squeeze the workers.

207
00:19:45,475 --> 00:19:48,500
And they should hang those capitalists.

208
00:20:02,450 --> 00:20:12,700
The Keizersgracht is left alone, but other squats get attacked.

209
00:20:57,190 --> 00:21:02,721
This set of evictions where the squatters only showed passively resistance ended up in violence.

210
00:21:02,721 --> 00:21:06,253
This house was empty for a year and it was recently squatted.

211
00:21:06,253 --> 00:21:09,647
It was evicted by the riot police right after the squatting action.

212
00:21:09,647 --> 00:21:12,862
Police, once again, protects solely the interest of the owners.

213
00:21:12,862 --> 00:21:15,330
The squatters decide to not accept this anymore.

214
00:21:15,330 --> 00:21:19,478
A week later, the building gets squatted once again and cops turn up once again.

215
00:21:19,478 --> 00:21:22,020
With everything at hand, they push back the cops.

216
00:21:50,090 --> 00:21:56,072
In order to avoid the eviction, massive barricades are built spontaneously in the street.

217
00:21:56,072 --> 00:21:59,770
In a few hours, the Free State of Vondel is proclaimed.

218
00:22:19,240 --> 00:22:28,133
We have squatted the building at the intersection of Vondelstraat in order to live in it.

219
00:22:28,133 --> 00:22:36,470
We built the barricades to defend ourselves from the illegal attacks from the cops.

220
00:22:36,470 --> 00:22:46,820
When the City Hall warranty us that we can remain in our houses, we won't need the barricades anymore. 

221
00:22:46,820 --> 00:22:54,860
The simple demands of the squatters are not met and the tanks arrive on Monday morning.

222
00:23:36,860 --> 00:23:40,810
I'm not at all happy about using violence against the police.

223
00:23:40,810 --> 00:23:43,924
I don't like at all to consciously hurt someone.

224
00:23:43,924 --> 00:23:49,057
In the end it's sad that you have to fight other people to get a place to live.

225
00:23:49,057 --> 00:23:54,788
It should not be like this at all.  But if at a given moment, as I saw at Vondeelstraat,

226
00:23:54,788 --> 00:23:59,229
when I saw the cops trying to run over people, that's so outrageous,

227
00:23:59,229 --> 00:24:04,280
that you want to do something about it. Then you grab whatever is near by you

228
00:24:04,280 --> 00:24:09,502
and you throw it to the police vans.  I also did that myself, that's so absurd.

229
00:24:09,502 --> 00:24:15,980
You basically end up ran over in the end. I can imagine that people get more and more aggressive. 

230
00:24:15,980 --> 00:24:20,619
At the Vondelstraat the squatters learned to hit back.

231
00:24:20,619 --> 00:24:23,629
The police face tougher resistance.

232
00:24:23,629 --> 00:24:30,814
The press and the politicians try to shift attention away from the housing shortage

233
00:24:30,814 --> 00:24:36,397
and focus on the public order issues. The police change tactics.

234
00:24:36,397 --> 00:24:41,302
They start using tear and vomiting gases. They try to break the squatters resistence.

235
00:24:41,302 --> 00:24:43,730
They form snatch teams to perform arrests.

236
00:25:41,780 --> 00:25:47,290
As a way of settling an example, massive arbitrary arrests are performed

237
00:25:47,290 --> 00:25:51,270
and people are locked away for long periods of time.

238
00:27:11,910 --> 00:27:17,786
Release all those arrested! Freedom fro Johannes! Freedom for Joost!

239
00:27:17,786 --> 00:27:24,569
Freedom for Erik! Freedom for Martin! Freedom for Henk! Free all the arrestes!

240
00:27:24,569 --> 00:27:30,750
Because he defended a house, Johanness got locked in jail for 2 months.

241
00:27:31,000 --> 00:27:35,853
What do you think of your mother being so active on your case?

242
00:27:35,853 --> 00:27:41,401
I wasn't surprised by her support. She knows very well what I'm up to.

243
00:27:41,401 --> 00:27:43,786
She believes in the squatters.

244
00:27:43,786 --> 00:27:48,254
But it was a surprise to see her along the other mothers

245
00:27:48,254 --> 00:27:51,371
getting so compromised in the struggle,

246
00:27:51,371 --> 00:27:54,655
doing radio shows, demonstrating, and so.

247
00:27:54,655 --> 00:27:57,460
That was really nice and charming. 

248
00:27:57,460 --> 00:28:01,206
He's at jail accused of open violence, domestic disturbance.

249
00:28:01,206 --> 00:28:07,002
At least this last one is ridiculous, the house was legally squatted and illegally evicted.

250
00:28:07,002 --> 00:28:12,756
And about the violence it's just because a single cop declared he saw him throwing stones.

251
00:28:12,756 --> 00:28:17,183
In total honesty, the cop is a really skilled man if he can recognize

252
00:28:17,183 --> 00:28:20,282
a single person among another 100 or 200 people.

253
00:28:20,282 --> 00:28:24,971
The cop gave a description of people that never matched up and later on,

254
00:28:24,971 --> 00:28:28,620
he changed his declaration after meeting Johannes twice.

255
00:28:28,620 --> 00:28:36,159
The unsual thing was that we were held for an extra 30 days.

256
00:28:36,159 --> 00:28:41,060
It was the first time they'd done that.

257
00:28:41,060 --> 00:28:49,600
I could have never imagine this rape happening in our land. 

258
00:28:49,600 --> 00:28:53,672
Despite of the increasing repression, the squatters keep on squatting.

259
00:28:53,672 --> 00:28:58,226
They've proven the existence of empty houses and now they turn their attention

260
00:28:58,226 --> 00:29:02,745
to the blocks of luxury flats which remain unsold because of the high prices.

261
00:29:02,745 --> 00:29:05,947
Squatting is getting more and more a political aspect.

262
00:29:05,947 --> 00:29:08,740
52 flats get squatted on the Prins Hendrikkade.

263
00:29:08,740 --> 00:29:13,714
You've squatted one of these luxury flats. Why not renting a room?

264
00:29:13,714 --> 00:29:20,579
There aren't any. In the last 12 months I've been in 6 places, but I could not afford them.

265
00:29:20,579 --> 00:29:25,103
I have a declaration for urgent housing for already 2 years.

266
00:29:25,103 --> 00:29:29,637
2 years I've been signed up at 2 different housing agencies,

267
00:29:29,637 --> 00:29:35,615
but there's nothing available. At least not for less than 1000 Gilders a month.

268
00:29:35,615 --> 00:29:39,629
I can't afford that, I don't have that kind of money.

269
00:29:39,629 --> 00:29:45,624
These building have been squatted because they are empty and people is in need.

270
00:29:45,624 --> 00:29:49,653
There's something like 1100 empty flats in Amsterdam.

271
00:29:49,653 --> 00:29:53,917
They are all for sale but no one can afford to buy them.

272
00:29:53,917 --> 00:29:58,335
- Don't you feel guilty about squatting an expensive flat?

273
00:29:58,335 --> 00:30:02,998
- Not at all. If  I were not squatting it, it would be empty.

274
00:30:02,998 --> 00:30:06,286
At most, maybe other squatters would be in.

275
00:30:06,286 --> 00:30:13,268
But otherwise they would be empty, they cannot be sold, they were already empty for 1 year.

276
00:30:13,268 --> 00:30:16,723
-Did squatting had a big effect on your life?

277
00:30:16,723 --> 00:30:23,585
It takes a lot of time, especially if you have a job. It's not only that I'm living here,

278
00:30:23,585 --> 00:30:28,298
it's also that you need to keep a look-out, make sure no cops

279
00:30:28,298 --> 00:30:31,627
nor gangs walk in and try to throw you out.

280
00:30:31,627 --> 00:30:38,790
And this year, for instance, I won't go on holidays because the situation does not allow it.

281
00:30:38,790 --> 00:30:42,003
The situation is too tense at the moment.

282
00:30:42,003 --> 00:30:47,212
We don't know what the court will decide or if we will be evicted.

283
00:30:47,212 --> 00:30:52,820
It could happen any day. So considering this, you cannot take holidays.

284
00:30:52,820 --> 00:31:01,696
Good evening.  The squatters from the 47 luxury flats in Prins Hendrikkade

285
00:31:01,696 --> 00:31:09,853
will have to leave in 2 weeks. This is the decision from the judge,

286
00:31:09,853 --> 00:31:17,530
he's order to evict the place, with the use of force if needed.

287
00:31:27,740 --> 00:31:40,743
Squatters' groups meet to discuss the impeding confrontation.

288
00:31:40,743 --> 00:31:45,860
Can violence be avoided?

289
00:31:45,860 --> 00:31:50,694
We've tried to make it clear, also in media, that we don't want violence.

290
00:31:50,694 --> 00:31:55,670
We always try to prevent it. But this problem is also related with tonight.

291
00:31:55,670 --> 00:31:58,068
People must say where do they stand.

292
00:31:58,068 --> 00:32:03,547
How many are ready to run the very great risk, either inside or outside the house?

293
00:32:03,547 --> 00:32:07,060
Who's willing to be arrested and or suffer violence?

294
00:32:07,060 --> 00:32:13,131
But in the end, this violence, that will come from the use of force from the police side,

295
00:32:13,131 --> 00:32:15,860
it's not coming from the squatters side.

296
00:32:15,860 --> 00:32:21,358
It was a really important decision by the squatters's assembly that we said:

297
00:32:21,358 --> 00:32:26,640
let them use the violence, we won't be provoked, we won't play this game.

298
00:32:26,640 --> 00:32:32,207
On the exterior, the squatters give the impression that there will be a battle.

299
00:32:32,207 --> 00:32:36,826
At the day of the eviction, every squatter is out except for one.

300
00:32:36,826 --> 00:32:40,100
2000 riot cops come to evict a single person. 

301
00:32:40,100 --> 00:32:49,262
You have not obeyed the bailiff's order to leave.

302
00:32:49,262 --> 00:32:55,620
You will not be evicted by force. 

303
00:32:55,620 --> 00:33:02,402
I can hardly believe what I see. An amazing display of police power.

304
00:33:02,402 --> 00:33:10,714
We've proven something quite clearly:  we are not going to be provoked by all this.

305
00:33:10,714 --> 00:33:15,752
That's why we decided not to have a confrontation.

306
00:33:15,752 --> 00:33:20,436
We remain on the struggle under our own terms.

307
00:33:20,436 --> 00:33:26,049
We'll choose how and when we'll continue the struggle. 

308
00:34:02,690 --> 00:34:06,840
Opinions were very divided after the Prins Hendrikkade.

309
00:34:06,840 --> 00:34:12,435
Some people said we should have resisted, but it was obvious that hundreds

310
00:34:12,435 --> 00:34:18,267
would have been injured or arrested. Army violence was huge at that eviction.

311
00:34:18,267 --> 00:34:23,279
They had 4 shooters with automatic rifles aiming at the squatters.

312
00:34:23,279 --> 00:34:29,149
Those guns fire 850 shots a minute. The water cannon throws water at 420Bars,

313
00:34:29,149 --> 00:34:33,977
they could have blown off the people at the roof, killing them.

314
00:34:33,977 --> 00:34:38,537
Then you just have to stop and think whether it's worth it.

315
00:34:38,537 --> 00:34:44,420
In the end, the sided on this topic. Was the struggle worth these casualties?

316
00:34:44,420 --> 00:34:50,029
The squatters have drawn attention to the housing shortage,

317
00:34:50,029 --> 00:34:56,036
but it's doubtful whether policy has changed as a result of it.

318
00:34:56,036 --> 00:35:01,759
The City hall bought the Keizersgracht from the speculators

319
00:35:01,759 --> 00:35:09,738
so a battle there was avoided. But elsewhere, squatters continue facing evictions.

320
00:35:09,738 --> 00:35:15,206
Decent flats are demolished to make way for speculation.

321
00:35:15,206 --> 00:35:20,998
Squatters neither find support at political national level.

322
00:35:20,998 --> 00:35:26,928
A new law is prepared in order to avoid house sitting empty,

323
00:35:26,928 --> 00:35:31,725
but in the end, the law makes squatting illegal.

324
00:35:31,725 --> 00:35:36,430
Banks supporting this law face a hard reaction.

325
00:35:36,430 --> 00:36:04,140
- I don't think this demo is worth it. - So why are you here? I'm against the new law and it's important to oppose it. But it's also important to show how the law is useless. They want us to throw stones and riot. This is not a solution, we could demonstrate in different ways.

326
00:36:04,140 --> 00:36:07,335
- This law won't change anything.

327
00:36:07,335 --> 00:36:14,500
I believe you can change laws by mixing demonstrating and direct action.  

328
00:36:14,500 --> 00:36:19,747
- Do you think demonstrating has any effect?  - It's one way, but not the only one.

329
00:36:19,747 --> 00:36:24,300
It no good just demonstrating, it's also important to keep on squatting.

330
00:36:24,300 --> 00:36:27,817
Means are chosen depending on the goal.

331
00:36:27,817 --> 00:36:33,271
So far, against the new law, only demonstrations take place.

332
00:36:33,271 --> 00:36:38,460
The banks who back the speculator get tougher treatment. 

333
00:36:55,840 --> 00:36:59,687
The fact is there's also a discussion inside the movement about

334
00:36:59,687 --> 00:37:03,400
the role of violence and how far will be used in the future.

335
00:37:03,400 --> 00:37:08,300
I believe nobody likes it,  but we see ourselves forced to chose these methods.

336
00:37:08,300 --> 00:37:13,900
I believe it's going the get heavy, really heave. There's no other way. 

337
00:37:13,900 --> 00:37:18,558
If we are going to take action, we'd better know the risks we are facing.

338
00:37:18,558 --> 00:37:22,141
We'd better be prepared to face arrest and imprisonment.

339
00:37:22,141 --> 00:37:27,169
Yes, this is the way things are going. And if we want to stay on the struggle,

340
00:37:27,169 --> 00:37:29,990
we'd better be ready. and think about this.

341
00:37:29,990 --> 00:37:33,940
We'd better reduce risks and always keep focus on our goals.

342
00:37:33,940 --> 00:37:37,169
People will squat as long as there's a housing shortage.

343
00:37:37,169 --> 00:37:40,860
As as long riot cops evict, there will be resistance against it.

344
00:37:40,860 --> 00:37:47,937
As long as I can't get a place legally, I'll go on squatting.

345
00:37:47,937 --> 00:37:54,078
- The new law will make it more difficult, won't it?

346
00:37:54,078 --> 00:38:00,700
Yes, but if enough people carry on, it'll work alright. 

347
00:38:00,700 --> 00:38:03,213
- What's the squatters answer to the new law?

348
00:38:03,213 --> 00:38:05,352
Kraken gaat door! (squatting goes on).

349
00:38:05,352 --> 00:38:09,580
Whatever law they pass, but for us, nothing changes, squatting will go on. 

